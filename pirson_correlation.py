from math import sqrt


def get_pirson_correlation(group1, group2):
    sum_x = sum_y = sum_xy = sum_xx = sum_yy = 0
    n = len(group1)

    if len(group1) != len(group2):
        raise Exception('len of params must be the same')

    for i in range(n):
        sum_x += group1[i]
        sum_y += group2[i]
        sum_xy += group1[i] * group2[i]
        sum_xx += group1[i]**2
        sum_yy += group2[i]**2

    numerator = sum_xy - (sum_x * sum_y / n)
    denominator = (sqrt(sum_xx - (sum_x * sum_x / n))) * (sqrt(sum_yy - (sum_y * sum_y / n)))

    if denominator == 0:
        return 0
    return numerator / denominator
