from user_dataset import users
from user_dataset import normalize_user
import operator
import pirson_correlation as pirson
from collections import OrderedDict


def minkowski(group1, group2, precision=10):
    if precision <= 0:
        return 0

    similarity = 0
    for key in group1:
        if key in group2:
            similarity += abs(group1[key] - group2[key]) ** precision

    similarity **= 1 / precision
    return similarity


def find_similar(to_user, all_users, count=1):
    res = {}
    if len(all_users) < 1:
        return res

    for user in all_users:
        if all_users[to_user] != all_users[user]:
            res[user] = minkowski(all_users[user], all_users[to_user])

    return sorted(res.items(), key=operator.itemgetter(1))[0:count]

#result = find_similar('Angelica', users, 10)
#print(result)
#print(sorted(users[result[0][0]], key=lambda item: item[1], reverse = True))

chan = list(dict.values(OrderedDict(normalize_user('Dan', 'Chan'))))
dan = list(dict.values(OrderedDict(users['Dan'])))

print(pirson.get_pirson_correlation(dan,chan))
